build js files

    npm run-script build-ts

test js files

    npm start

build docker image

    docker build -t mbptxutw/arbitrage:latest .

test docker image

    docker run mbptxutw/arbitrage:latest

upload docker image

    docker push mbptxutw/arbitrage:latest

create compute engine instance with start script `docker run mbptxutw/arbitrage:latest` and cointainer-optimized OS (stable).

ssh into compute engine, `docker login` and

    docker pull mbptxutw/arbitrage:latest

restart compute engine.