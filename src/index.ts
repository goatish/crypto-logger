import * as admin from 'firebase-admin'
import { gdax, bitstamp } from 'ccxt';
const serviceAccount = require("../firebase-adminsdk.json")

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://arbitrage-d269e.firebaseio.com/"
});

const db = admin.database()
const data_ref = db.ref("data")
const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms))

class Market {
    symbol: string
    id: string
    bid: number
    ask: number
    timestamp: number

    constructor(symbol: string, id: string) {
        this.symbol = symbol
        this.id = id
    }

    public insert = function (data): Market {
        this.bid = data.bid
        this.ask = data.ask
        this.timestamp = data.timestamp
        return this
    }
}

class Exchange {
    eth_eur: Market = new Market('ETH/EUR', 'eth_eur')
    ltc_eur: Market = new Market('LTC/EUR', 'ltc_eur')
    btc_eur: Market = new Market('BTC/EUR', 'btc_eur')
    id: string
    ccxt

    constructor(id) {
        this.id = id

        if (this.id === 'gdax') this.ccxt = new gdax()
        if (this.id === 'bitstamp') this.ccxt = new bitstamp()
    }

    public insert = function (market: string, data): Market {
        if (market === 'ETH/EUR') return this.eth_eur.insert(data)
        if (market === 'LTC/EUR') return this.ltc_eur.insert(data)
        if (market === 'BTC/EUR') return this.btc_eur.insert(data)
        throw new Error('Trying to insert data for a market that does not exist')
    }
}

class Data {
    gdax: Exchange = new Exchange('gdax')
    bitstamp: Exchange = new Exchange('bitstamp')
}


async function logger() {
    const markets = ['ETH/EUR', 'LTC/EUR', 'BTC/EUR'],  // observed currency pairs
        data: Data = new Data(), 		                // cache current ticker prices
        minNotification: number = 0                    // minimum exploitable price difference between exchanges

    /* get ticker prices for all markets and exchanges */
    while (true) {
        for (const market of markets) {
            await Promise.all([getTicker(data.gdax, market), getTicker(data.bitstamp, market)])
                .catch(reason => {
                    console.log(reason);
                });
            await sleep(500)
        }
    }

    function comparePrices(market: Market, exchange: Exchange) {
    	/*
    		Compare new prices from one exchange with
    		saved prices from all other exchanges.
    	*/

        const newBid: number = market['bid'],
            newAsk: number = market['ask']

        let otherExchange: Exchange

        if (exchange.id === 'gdax') otherExchange = data.bitstamp
        else if (exchange.id === 'bitstamp') otherExchange = data.gdax
        else throw new Error('Exchange id unknown')

        const otherBid: number = otherExchange[market.id]['bid'],
            otherAsk: number = otherExchange[market.id]['ask'],
            timestamp: number = otherExchange[market.id]['timestamp'];

        logDifference(exchange, otherExchange, otherBid, newAsk);
        logDifference(otherExchange, exchange, newBid, otherAsk);


        function logDifference(from: Exchange, to: Exchange, bid: number, ask: number) {
            /* 
                Verkaufpreis / Kaufpreis = K1 / K0 
                
                Ask = Verkäufer, Bid = Käufer

                fee = 0.0025
                factor = min(diff, 1.01)
                buyLimit = bid + diff - (toFee + fromFee)
                sellLimmit = buyLimit + toFee
            */

            const diff: number = bid / ask,
                factor: number = diff < 1.01 ? diff : 1.01,
                buyLimit: number = ask + 0.333 * (bid-ask),
                sellLimit: number = bid - 0.333 * (bid-ask)

            let warning:string = "OK" 
            if (((bid-ask) * 0.33) < bid * 0.005) warning = "BAD"

            const msg = {
                buyLimit: precisionRound(buyLimit, 2),
                sellLimit: precisionRound(sellLimit, 2),
                percentDifference: precisionRound((diff - 1) * 100, 2),
                fromExchange: from.id,
                toExchange: to.id,
                timestamp: timestamp,
                market: market.symbol,
                warning: warning
            }

            if (diff > 1) data_ref.child(market.id).update(msg)

            // if (diff > minNotification) notify(msg);

            function notify(message) {
                // The topic name can be optionally prefixed with "/topics/".
                const topic = "arbitrage-over-two";

                // See the "Defining the message payload" section below for details
                // on how to define a message payload.
                const payload = {
                    notification: {
                        title: "Arbitrage over 2%",
                        body: message
                    }
                };

                const options = {
                    collapseKey: "arbitrage-over-two",
                    timeToLive: 60 * 30
                }

                // Send a message to devices subscribed to the provided topic.
                admin.messaging().sendToTopic(topic, payload, options)
                    .then(function (response) {
                        // See the MessagingTopicResponse reference documentation for the
                        // contents of response.
                        console.log("Successfully sent message:", response);
                    })
                    .catch(function (error) {
                        console.log("Error sending message:", error);
                    });
            }

            function precisionRound(number: number, precision: number): number {
                const precFactor: number = Math.pow(10, precision);
                return Math.round(number * precFactor) / precFactor;
            }

        }
    }

    async function getTicker(exchange: Exchange, market: string): Promise<void> {
    	/*
    		Get ticker prices for one market of one exchange.
    		Save prices and notify the compare-function of new data.
    	*/
        let ticker = null,
            sleeptime: number = 2000

        while (!ticker) {
            try {
                ticker = await exchange.ccxt.fetchTicker(market);
            }
            catch (err) {
                console.log(exchange.id + ' unreachable. Waiting ', sleeptime, 'ms until next try.');
                await sleep(sleeptime);
                if (sleeptime < 10000) { sleeptime += 500 };
            }
        }

        const target_market: Market = exchange.insert(market, {
            bid: ticker.bid,
            ask: ticker.ask,
            timestamp: ticker.timestamp
        })

        comparePrices(target_market, exchange);
    }
}

logger();