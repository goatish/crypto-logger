# Use node 8.9.4 LTS
FROM node:8.9.4
ENV NODE_ENV=production

# Copy source code
COPY . /app

# Change working directory
WORKDIR /app

# Install dependencies
RUN npm install

# Launch application
CMD ["npm","start"]

# At the end, set the user to use when running this image
USER node